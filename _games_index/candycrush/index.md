---
title: Candy Crush
short_description: A variant of Bejeweled.
---

![](../candycrush/candycrush.png){:width="300"}

## Description

[Candy Crush Saga][2] is a single-player videogame whose game mechanic is based on
the idea of swapping adjacent items of a grid in order to form rows or colums of
multiple items of the same kind.
It can be considered a variant of [Bejeweled]({{site.baseurl}}/i/bejeweled/). 


## Computational Complexity 

See the [CoG page on Bejeweled]({{site.baseurl}}/i/bejeweled/).


## Notes

A playable version of the NP-hardness reduction of [[1]] is available [here]({{site.baseurl}}/g/bejeweled/). 

## References

[[1]] L. Guala, S. Leucci, E. Natale, "Bejeweled, Candy Crush and other match-three games are (NP-)hard", in CIG 2014.


[1]: https://ieeexplore.ieee.org/document/6932866/
[2]: https://en.wikipedia.org/wiki/Candy_Crush_Saga





