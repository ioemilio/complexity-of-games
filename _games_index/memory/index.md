---
title: Memory Solitaire
short_description: The Memory Solitaire game consists in flipping pairs of cards laid face down and finding matches.
---

![](memory.png){:width="320"}

## Description

Memory Solitaire is played with a deck of $n$ pairs of cards. 
Each pair is composed of identical cards. 
The deck is shuffled and the cards are laid face down. 
A move of the player consists of sequentially choosing two cards to flip over, i.e., the player flips the first card before choosing the second one. 
If the cards match, they are removed from the game; otherwise, they are both flipped back over. 
A game ends when all pairs have been matched.

## Computational complexity

The game belongs to the complexity class $P$. 
However its complexity has been studied more in detail.
The first two results [[1]],[[2]] assume a perfect memory of the player after he flips a card for the whole game.
The focus of the two papers is on the expected number of moves needed to complete the game using the optimal strategy.

In [[1]] non-tight lower and upper bounds are given: It is shown that, in expectation, every strategy needs at least $(1.5 \cdot n - 1)$ moves and the optimal strategy needs at most $(1.75 \cdot n)$ moves.
They also conjecture that the optimal strategy has an expected number of moves of $\approx(1.61 \cdot n),$ supporting it with numerical calculations.
In [[2]] the conjecture is closed: Tight lower and upper bounds matching the $(1.61 \cdot n)$ are given.

In [[3]] a different setting where the player does not have perfect memory is analyzed. In particular tight time-space tradeoff bounds are given. Let $T$ be the number of cards flipped and $S$ be the maximum number of bits that the player can use. The authors show that $ST=\Theta(n^2 \log n).$
They also introduce a more general model in which the player cannot only check for equality of the cards he flips, but do arbitrary computations. 
In this model they prove that $ST^2=\Omega(n^3),$ however they conjecture that a stronger tradeoff $ST=\tilde\Omega(n^2)$ holds even in this more general model.

## References

[[1]] {% include warning_peer_review.html %} K. Foerster and R. Wattenhofer, "The Solitaire Memory Game". 2013.

[[2]] D.J. Velleman and G.S. Warrington, "What to expect in a game of memory." The American Mathematical Monthly. 2013.

[[3]] {% include warning_peer_review.html %} A. Chakrabarti and C. Yining. "Time-Space Tradeoffs for the Memory Game." arXiv preprint. 2017.

[1]: https://ktfoerster.github.io/paper/2013-memory.pdf
[2]: https://www.tandfonline.com/doi/abs/10.4169/amer.math.monthly.120.09.787
[3]: https://arxiv.org/abs/1712.01330